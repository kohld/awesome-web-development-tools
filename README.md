# Awesome Web-Development tools (PHP and JavaScript)

This is a personal list of awesome tools which improves my workflows.

<!-- TOC -->

- [Awesome Web-Development tools (PHP and JavaScript)](#awesome-web-development-tools-php-and-javascript)
    - [Software](#software)
        - [Helpful Extensions for Chrome](#helpful-extensions-for-chrome)
        - [Helpful Plugins for Terminals](#helpful-plugins-for-terminals)
        - [Helpful Plugins for PhpStorm](#helpful-plugins-for-phpstorm)
            - [Code Quality and Support](#code-quality-and-support)
            - [Themes](#themes)
            - [Utilities](#utilities)
        - [Helpful Plugins for Visual Studio Code](#helpful-plugins-for-visual-studio-code)
            - [General Plugins](#general-plugins)
            - [Plugins for PHP](#plugins-for-php)
            - [Plugins for JavaScript](#plugins-for-javascript)

<!-- /TOC -->

## Software

* [Bear](https://bear.app/) - (**macOS**) Bear is a beautiful, flexible writing app for crafting notes and prose.
* [ChromeDriver](http://chromedriver.chromium.org/) - ChromeDriver is an open source tool for automated testing of webapps across many browsers.
* [Fork](https://git-fork.com/) - A fast and friendly git client for Mac and Windows like SourceTree.
* [Homebrew](https://brew.sh/) - (**macOS**) A simple and beautiful package manager for macOS, to install packages clean, like 'PHP', 'tree' and so on.
* [Insomnia](https://insomnia.rest/) - Free and powerful HTTP and GraphQL tool belt as a good alternative for postman.
* [iTerm2](https://www.iterm2.com/) - (**macOS**) iTerm2 is a replacement for Terminal and the successor to iTerm.
* [Kitematic](https://kitematic.com/) - Docker desktop solution.
* ['Oh My ZSH!'](http://ohmyz.sh/) - Oh-My-Zsh is an open source, community-driven framework for managing your ZSH configuration via your terminal, which comes bundled with a lot of helpful functions, helpers, plugins, themes, and a few things that make you shout. My tip: try the theme agnoster with iTerm2  ('non-ascii font' as setting)
* [PhpStorm](https://www.jetbrains.com/phpstorm/) - PhpStorm is a commercial, cross-platform IDE for PHP, HTML and JavaScript with on-the-fly code analysis, error prevention and automated refactorings for PHP and JavaScript code.
* [Rambox](https://rambox.pro/) - Rambox is a workspace browser that allows you to manage as many applications as you want, all in one place.
* [Sequel Pro](https://www.sequelpro.com/) - Sequel Pro is a fast, easy-to-use Mac database management application for working with MySQL databases.
* [SourceTree](https://www.sourcetreeapp.com/) - A Git GUI that offers a visual representation of your repositories.
* [Visual Studio Code](https://code.visualstudio.com/) - Visual Studio Code is a streamlined code editor with support for development operations like debugging, task running and version control.

### Helpful Extensions for Chrome

* [Bitbucket Diff Tree](https://chrome.google.com/webstore/detail/bitbucket-diff-tree/pgpjdkejablgneeocagbncanfihkebpf) - Extension to display diff tree for Bitbucket.
* [Octotree](https://chrome.google.com/webstore/detail/octotree/bkhaagjahfmjljalopjnoealnfndnagc) - Chrome extension that displays a code tree on GitHub. Great for exploring project source code without having to download many repositories to your machine.

### Helpful Plugins for Terminals

* [fzf](https://github.com/junegunn/fzf) - A general-purpose command-line fuzzy finder.
* [TheFuck](https://github.com/nvbn/thefuck) - Magnificent app which corrects your previous console command.

### Helpful Plugins for PhpStorm

#### Code Quality and Support

* [Php Inspections (EA Extended)](https://plugins.jetbrains.com/plugin/7622-php-inspections-ea-extended-) - This is a static code analysis plugin for PHP.
* [Symfony Plugin](https://plugins.jetbrains.com/plugin/7219-symfony-plugin) - A lot of small useful tweaks for e.g. Symfony, doctrine and twig.

#### Themes

* [Material UI](https://plugins.jetbrains.com/plugin/8006-material-theme-ui) - Adds the [Material Theme](https://github.com/equinusocio/material-theme) look and bevaior to your IDE.

#### Utilities

* [Nyan Progress Bar](https://plugins.jetbrains.com/plugin/8575-nyan-progress-bar) - Pretty progress bar for PHPStorm. Just....amazing. Happy loading!
* [PHP Annotations](https://plugins.jetbrains.com/plugin/7320-php-annotations) - Adds support for PHP annotations. (Awesome in combination with the Symfony Plugin!)
* [PHP Toolbox](https://plugins.jetbrains.com/plugin/8133-php-toolbox) - Adds completion and typing for many libraries using dynamic returns types based on arguments.

### Helpful Plugins for Visual Studio Code

#### General Plugins

* [Docker - Microsoft](https://marketplace.visualstudio.com/items?itemName=PeterJausovec.vscode-docker) - Adds syntax highlighting, snippets, commands, hover tips, and linting for Dockerfile and docker-compose files.
* [Git Blame - Wade Anderson](https://marketplace.visualstudio.com/items?itemName=waderyan.gitblame) - Get and see the git commit information in the status bar for the currently selected line of code.
* [Git History (git log) - Don Jayamanne](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory) - View git log, file or line history inside of VS Code.
* [GitLab Workflow - Fatih Acet](https://marketplace.visualstudio.com/items?itemName=fatihacet.gitlab-workflow) - This extension integrates GitLab to VSCode by adding GitLab specific options to VSCode command palette and status bar.
* [Prettier - Code formatter - Esben Petersen](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) - VS Code package to format your JavaScript / TypeScript / CSS using Prettier.
* [Sort lines - Daniel Imms](https://marketplace.visualstudio.com/items?itemName=Tyriar.sort-lines) - Sort marked lines of text. Perfect for translation files!
* [SynthWave '84 - Robb Owen](https://marketplace.visualstudio.com/items?itemName=RobbOwen.synthwave-vscode) - A Synthwave-inspired colour theme to satisfy your neon dreams. Amazing glow effects!
* [TODO Highlight - Wayou Liu](https://marketplace.visualstudio.com/items?itemName=wayou.vscode-todo-highlight) - Highlight TODO,FIXME or any annotations within your code.
* [VSCode Great Icons - Emmanuel Béziat](https://marketplace.visualstudio.com/items?itemName=emmanuelbeziat.vscode-great-icons) - A big pack of icons (100+) for your files.

#### Plugins for PHP

* [PHP Debug - Felix Becker](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug) - Simple debug support for PHP with e.g. XDebug.
* [PHP Extension Pack - Felix Becker](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-pack) - Includes the most important extensions to get you started with PHP development in Visual Studio Code.
* [PHP IntelliSense - Felix Becker](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-intellisense) - Advanced autocompletion and refactoring support for PHP.

#### Plugins for JavaScript

* [Beautify - HookyQR](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify) - Beautify code (JavaScript, JSON, CSS, Sass, and HTML) in place for VS Code.
* [ESLint - Dirk Baeumer](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) - Integrates ESLint into VS Code.
* [Import Cost - Wix](https://marketplace.visualstudio.com/items?itemName=wix.vscode-import-cost) - Display import/require package size in the editor.
* [vscode-styled-components - Julien Poissonnier](https://marketplace.visualstudio.com/items?itemName=jpoissonnier.vscode-styled-components) - Syntax highlighting and IntelliSense for styled-components.